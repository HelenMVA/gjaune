import { Component, OnInit, Input } from '@angular/core';
import { jauneModel } from '../model/jauneModel';
import { Observable } from 'rxjs';
import { JauneService } from '../jaune.service';
import { FormBuilder, FormGroup } from '@angular/forms';

declare var M: any;

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  creationForm: FormGroup;
  getGJ$: Observable<jauneModel[]>;

  constructor(private gJaunesvc: JauneService, private fb: FormBuilder) {}

  ngOnInit() {
    this.getGJ$ = this.gJaunesvc.getGiletJaune();
    this.createFormGj();
  }
  createFormGj() {
    this.creationForm = this.fb.group({
      pseudo: '',
      age: '',
      email: '',
      pass: '',
      role: ''
    });
  }

  getInfo() {
    this.gJaunesvc.getGiletJaune().subscribe(data => {
      console.log(data);
    });
  }

  delete(jg: jauneModel) {
    this.gJaunesvc.deleteJG(jg._id).subscribe(data => {
      console.log(data);
      location.reload();
    });
  }

  onEdit(jg: jauneModel) {
    this.gJaunesvc.selectedmodel = jg;

    this.gJaunesvc.putEmployee(this.creationForm.value).subscribe(res => {
      this.refreshEmployeeList();
      M.toast({ html: 'Updated successfully', classes: 'rounded' });
    });
  }

  refreshEmployeeList() {
    this.gJaunesvc.getGiletJaune().subscribe(res => {
      this.gJaunesvc.modelnew = res as jauneModel[];
    });
  }
}
