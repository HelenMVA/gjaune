import { TestBed } from '@angular/core/testing';

import { JauneService } from './jaune.service';

describe('JauneService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: JauneService = TestBed.get(JauneService);
    expect(service).toBeTruthy();
  });
});
