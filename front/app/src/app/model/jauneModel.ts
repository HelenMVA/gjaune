export interface jauneModel {
  _id: string;
  pseudo: String;
  age: Number;
  email: String;
  pass: String;
  role: String;
}
