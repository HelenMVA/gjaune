import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  @Input() gjData;
  constructor() {}

  ngOnInit() {}
  isShow = true;
  isShow2 = false;

  toggleDisplay() {
    // this.isShow = !this.isShow;
    this.isShow = true;
    this.isShow2 = false;
  }
  toggleDisplay2() {
    // this.isShow = !this.isShow;
    this.isShow2 = true;
    this.isShow = false;
  }
}
