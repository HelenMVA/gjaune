import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { JauneService } from '../jaune.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.scss']
})
export class InscriptionComponent implements OnInit {
  creationForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private gJaunesvc: JauneService,
    private router: Router
  ) {}

  ngOnInit() {
    this.createFormGj();
  }
  createFormGj() {
    this.creationForm = this.fb.group({
      pseudo: '',
      age: '',
      email: '',
      pass: '',
      role: ''
    });
  }

  postNewGj() {
    if (this.creationForm.valid) {
      console.log(this.creationForm);
      this.gJaunesvc.createGj(this.creationForm.value).subscribe(data => {
        console.log(data, 'Gj inscrit');
        this.router.navigate(['/']);
      });
    }
  }
}
