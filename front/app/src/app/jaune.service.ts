import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { jauneModel } from './model/jauneModel';

@Injectable({
  providedIn: 'root'
})
export class JauneService {
  selectedmodel:jauneModel;
  modelnew: jauneModel[];
  apiURL = 'http://localhost:8080/';

  constructor(private http: HttpClient) {}

  createGj(post: jauneModel) {
    console.log(post);
    return this.http.post<jauneModel>(`${this.apiURL}register`, post);
  }

  loginGj(connect: jauneModel) {
    console.log(connect);
    return this.http.post<jauneModel>(`${this.apiURL}login`, connect);
  }

  getGiletJaune(): Observable<jauneModel[]> {
    return this.http.get<jauneModel[]>(`${this.apiURL}recuperjg`);
  }

  deleteJG(id: string): Observable<jauneModel[]> {
    return this.http.delete<jauneModel[]>(`${this.apiURL}${id}`);
  }

  putEmployee(updatejaune: jauneModel) {
    return this.http.put(`${this.apiURL}${updatejaune._id}`, updatejaune);
  }
}
