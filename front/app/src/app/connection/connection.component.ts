import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { JauneService } from '../jaune.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-connection',
  templateUrl: './connection.component.html',
  styleUrls: ['./connection.component.scss']
})
export class ConnectionComponent implements OnInit {
  creationForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private giletJauneSvc: JauneService,
    private router: Router
  ) {}

  ngOnInit() {
    this.formLogin();
  }

  formLogin() {
    this.creationForm = this.fb.group({
      pseudo: '',
      pass: ''
    });
  }

  connectGJ() {
    if (this.creationForm.valid) {
      console.log(this.creationForm);
      this.giletJauneSvc.loginGj(this.creationForm.value).subscribe(data => {
        console.log(data);
        this.router.navigate(['/']);
      });
    }
  }
}
