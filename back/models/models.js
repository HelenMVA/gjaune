const mongoose = require('mongoose');

let jauneSchema = new mongoose.Schema({
  pseudo: String,
  age: Number,
  email: String,
  pass: String,
  role: String
});

module.exports = mongoose.model('testCrud', jauneSchema);
