const express = require('express');
const api = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const mongoose = require('./db.js');
const controller = require('./controllers/controllers.js');

api.use(bodyParser.urlencoded({ extended: false }));
api.use(bodyParser.json());
api.use(cors());

api.use('/', controller);

api.listen(8080, () => {
  console.log('on utilise port 8080');
});
