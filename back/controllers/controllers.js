const express = require('express');
const router = express.Router();
const model = require('../models/models.js');
const ObjectId = require('mongoose').Types.ObjectId;

router.post('/register', (req, res) => {
  console.log('ok');
  let jauneData = req.body;
  console.log(req.body);
  let postdbJG = new model(jauneData);

  postdbJG.save(err => {
    if (err) console.log(err);
    else console.log('gilet jaune inscrit');
    res.status(201).json(jauneData);
  });
});

router.post('/login', (req, res) => {
  let login = req.body;

  model.findOne(
    { pseudo: login.pseudo, pass: login.pass },
    (err, dataLogin) => {
      if (
        dataLogin === null ||
        dataLogin.pseudo !== login.pseudo ||
        dataLogin.pass !== login.pass
      ) {
        console.log('veillez verifier vos données');
        res.status(500).send();
      } else if (
        dataLogin.pseudo === login.pseudo &&
        dataLogin.pass === login.pass
      ) {
        console.log('on est connecté');
        res.send();
      }
    }
  );
});

router.get('/recuperjg', (req, res) => {
  model.find({}, (err, gj) => {
    if (err) {
      res.status(500).send('pas possible de récuperer les gilets jaune');
    } else {
      res.json(gj);
      console.log('on a get nos gj');
    }
  });
});

router.delete('/:id', (req, res) => {
  if (!ObjectId.isValid(req.params.id))
    return res.status(400).send(`ig n'est pas trouvé`);

  model.findByIdAndRemove(req.params.id, (err, doc) => {
    if (!err) {
      res.send(doc);
    } else {
      console.log('Error delete :' + JSON.stringify(err, undefined, 2));
    }
  });
});

router.put('/:id', (req, res) => {
  let giletObj = {};
  let data = req.body;

  giletObj.pseudo = data.pseudo;
  giletObj.age = data.age;
  giletObj.email = data.email;
  giletObj.pass = data.pass;
  giletObj.role = data.role;

  console.log(giletObj);

  model.findByIdAndUpdate(
    { _id: req.params.id },
    { $set: giletObj },
    { new: true },
    err => {
      if (err) return err;
      else res.status(204).send('gilet modifier');
    }
  );
});

module.exports = router;
